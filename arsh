#!/bin/sh

# --- shell options --- #

# same as -e
# exit immediately if command exits with a non-zero status
set -o errexit
# same as -u
# treat unset variables as error when substituting
set -o nounset

# --- shell options --- #

# --- shell variables --- #

script_path="$(dirname $(realpath ${0}))"

# --- shell variables --- #

# --- shell functions --- #

# check_if_installed(): a wrapper for `command -v` that silences the output and
# error streams.
#
# use:
#   check_if_installed cmd
check_if_installed() {
    command -v "${1}" 2>/dev/null 1>&2;
}

# check_if_installed_and_exit(): as the name implies, check if a program is installed. if
# it is, continue, otherwise exit with non-zero status.
#
# use:
#   check_if_installed_and_exit cmd
check_if_installed_and_exit() {
    if ! command -v "${1}" 2>/dev/null 1>&2; then
        printf '%s\n' "${1} is not installed. Please install ${1} to continue."
        exit 1
    fi
}

# check_if_root(): check if user is root.
#
# use:
#   check_if_root
check_if_root() {
    [ ${UID} = "0" ]
}

# check_if_root_and_exit(): check if user is root and if not, exit with an error message.
#
# use:
#   check_if_root_and_exit
check_if_root_and_exit() {
    if [ ${UID} != "0" ]; then
        printf '%s\n' "Not logged in as root. Exiting."
        exit 1
    fi
}

# run_cmd(): run commands while suppressing both the output and error streams
# and displaying a user-friendly "done" or "failed".
#
# use:
#   run_cmd cmd
run_cmd() {
    if [ -z "${1}" ]; then
        printf '%s' "No arguments provided. Exiting."
        exit 1
    fi

    # if ${logfile} is UNSET or EMPTY, set the value to the string "/dev/null"
    output_stream="${logfile:-/dev/null}"
    error_stream="${logfile:-/dev/null}"

    if ${@} 1>"${output_stream}" 2>"${error_stream}"; then
        printf " done\n"
    else
        printf " failed\n"
        exit 1
    fi
}

# run_cmd_with_log(): run commands while redirecting STDOUT and STDERR and
# displaying a user-friendly "done" or "failed" after the displayed message.
#
# use:
#   run_cmd logfile cmd
run_cmd_with_log() {
    if [ -z "${1}" ]; then
        printf '%s' "No arguments provided. Exiting."
        exit 1
    fi

    # get the absolute path of the first argument provided, i.e the file to log
    # stdout and stderr to
    logfile="$(realpath ${1})"

    # shift parameters by 1 to the right
    # $1=a $2=b $3=c
    #
    # become:
    #
    # $1=b $2=c $3=
    shift 1

    # reuse code in the run_cmd function
    run_cmd "${@}"
}

# status(): display a tick if the last command succeeded or a cross if the last
# command failed. this is done by cheching the exit code, which if non-zero
# indicates erroneous exit. basically, a drop-in replacement for `run_cmd()`
# where it cannot be used, for example: commands that need to be run with `sudo`
# or `doas`.
#
# use:
#   status
status() {
    if [ "${?}" = "0" ]; then
        printf " \u2714\n"
    else
        printf " \u274c\n"
        exit 1
    fi
}

# --- shell functions --- #

# --- pre-install starts --- #

# set_datetime(): enable network time protocol, set timezone to the given
# argument.
#
# use:
#   set_datetime "Asia/Kolkata"
set_datetime() {
    printf '%s' "Enabling NTP"
    run_cmd timedatectl set-ntp true

    printf '%s' "Setting timezone to ${1}"
    run_cmd timedatectl set-timezone "${1}"
}

# set_mirrors(): use mirrors closer to your country for better speeds.
#
# arguments:
#   1 -> first country code preference
#   2 -> second country code preference
#
# use:
#   set_mirrors "IN" "SG"
set_mirrors() {
    printf '%s' "Setting installation mirrors to ${1} and ${2}"
    run_cmd curl -s "https://archlinux.org/mirrorlist/?country=${1}&country=${2}&protocol=https&use_mirror_status=on" \
        | sed 's/^#//' \
        | tee /etc/pacman.d/mirrorlist
}

# wipe_disk(): assume it's a SSD if "--ssd" is passed and run a TRIM / DISCARD
# operation whichi informs the flash storage firmware that blocks of data are no
# longer in use and can be wiped internally. if it fails because either the
# firmware doesn't support a DISCARD sent from the OS or if it's not flash
# storage, use `sgdisk` to delete the partition table instead.
#
# use:
#   wipe_disk "/dev/sda"
#   wipe_disk --ssd "/dev/sdb"
wipe_disk() {
    printf '%s' "Wiping disk"
    if [ "${1}" = "--ssd" ]; then
       run_cmd blkdiscard "${2}"
    else
       run_cmd sgdisk --zap-all "${1}"
    fi
}

# mk_parttable(): creates a GUID partition table (GPT) on the supplied disk.
#
# use:
#   mk_parttable "/dev/sda"
mk_parttable() {
    printf '%s' "Creating a GUID partition table"
    run_cmd sgdisk --clear --mbrtogpt "${1}"
}

# mk_efi_part(): create an ESP (EFI System Partition).
#
# arguments:
#   1 -> partition number
#   2 -> size of the partition to be created
#   3 -> path to the disk
#
# use:
#   mk_efi_part "1" "512M" "/dev/sda"
mk_efi_part() {
    printf '%s' "Creating a EFI system partition"
    run_cmd sgdisk --new="${1}":0:+"${2}" --typecode="${1}":ef00 --change-name="${1}":"ESP" "${3}"
}

# mk_swap_part(): create a swap partition.
#
# arguments:
#   1 -> partition number
#   2 -> size of the partition to be created
#   3 -> path to the disk
#
# use:
#   mk_swap_part "2" "8G" "/dev/sda2"
mk_swap_part() {
    printf '%s' "Creating a swap partition"
    run_cmd sgdisk --new="${1}":0:+"${2}" --typecode="${1}":8200 --change-name="${1}":"SWAP" "${3}"
}

# mk_swap_file(): create a swap file, with dd.
#
# arguments:
#   1 -> path to the swapfile to be created
#   2 -> size of the swapfile in gibibytes
#
# use:
#   mk_swap_file "/mnt/swap" "4"
mk_swap_file() {
    printf '%s' "Creating swap file"
    run_cmd dd if=/dev/zero of="${1}" count="${2}" bs=1G conv=fdatasync

    printf '%s' "Setting permissions on swap file"
    run_cmd chmod 0600 "${1}"
}

# mk_root_part(): create a root partition that takes rest of the available
# space.
#
# arguments
#   1 -> partition number
#   2 -> path to the partition to be created
#
# use:
#   mk_root_part "3" "/dev/sda"
mk_root_part() {
    printf '%s' "Creating root partition"
    run_cmd sgdisk --new="${1}":0:0 --typecode="${1}":8300 --change-name="${1}":"ROOT" "${2}"
}

# mk_boot_fs(): format boot partition with FAT32.
#
# arguments:
#   1 -> path to the partition to be formatted
#
# use:
#   mk_boot_fs "/dev/sda1"
mk_boot_fs() {
    printf '%s' "Formatting boot partition with FAT32"
    run_cmd mkfs.vfat -F32 -n "BOOT" "${1}"
}

# mk_swap_fs(): format swap partition or file.
#
# arguments:
#   1 -> path to the partition to be formatted
#
# use:
#   mk_swap_fs "/dev/sda1"
mk_swap_fs() {
    printf '%s' "Formatting swap"
    run_cmd mkswap -fL "SWAP" "${1}"
}

# mk_crypt_fs(): format partition with LUKS2.
#
# arguments:
#   1 -> path to the key file
#   2 -> path to the partition to be formatted
#
# use:
#   mk_crypt_fs "/tmp/boot/keyfile" "/dev/sda3"
mk_crypt_fs() {
    printf '%s' "Creating LUKS2 partition"
    run_cmd cryptsetup luksFormat \
        --batch-mode \
        --key-file "${1}" \
        "${2}"
}

# open_crypt_drive(): decrypt a LUKS2 formatted drive with
# detached header.
#
# arguments:
#   1 -> path to the key file
#   2 -> path to the formatted drive
#   3 -> name of the decrypted drive
#
# use:
#   open_crypt_drive "/tmp/key" "/dev/sda3" "root"
open_crypt_drive() {
    printf '%s' "Opening crypt drive"
    run_cmd cryptsetup luksOpen \
        --key-file "${1}" \
        "${2}" "${3}"
}

# mk_crypt_fs_with_detached_header(): format partition with LUKS2 with detached
# header.
#
# arguments:
#   1 -> path to the header file
#   2 -> path to the key file
#   3 -> path to the partition to be formatted
#
# use:
#   mk_crypt_fs_with_detached_header "/tmp/boot/header" "/tmp/boot/keyfile" "/dev/sda3"
mk_crypt_fs_with_detached_header() {
    printf '%s' "Creating LUKS2 partition"
    run_cmd cryptsetup luksFormat \
        --batch-mode \
        --header "${1}" \
        --key-file "${2}" \
        "${3}"
}

# open_crypt_drive_with_detached_header(): decrypt a LUKS2 formatted drive with
# detached header.
#
# arguments:
#   1 -> path to the header file
#   2 -> path to the key file
#   3 -> path to the formatted drive
#   4 -> name of the decrypted drive
#
# use:
#   open_crypt_drive_with_detached_header "/tmp/header" "/tmp/key" "/dev/sda3" "root"
open_crypt_drive_with_detached_header() {
    printf '%s' "Opening crypt drive"
    run_cmd cryptsetup luksOpen \
        --header "${1}" \
        --key-file "${2}" \
        "${3}" "${4}"
}

# mk_ext4_fs(): format partition with ext4.
#
# arguments:
#   1 -> path to the partition to be formatted
#
# use:
#   mk_ext4_fs "/dev/sda3"
mk_ext4_fs() {
    printf '%s' "Formatting partition with ext4"
    run_cmd mkfs.ext4 \
        -E lazy_itable_init=0,lazy_journal_init=0 \
        -F \
        -L "ROOT" \
        -m 0.05 \
        -T huge \
        "${1}"
}

# mk_btrfs_fs(): format partition with btrfs.
#
# arguments:
#   1 -> path to the partition to be formatted.
#
# use:
#   mk_btrfs_fs "/dev/sda3"
mk_btrfs_fs() {
    printf '%s' "Formatting partition with btrfs"
    run_cmd mkfs.btrfs \
        --force \
        --quiet \
        "${1}"
}

# mount_part(): a wrapper around mount.
#
# arguments
#   1 -> path to the device to be mounted
#   2 -> path to the mountpoint
#
# use:
#   mount_part "/dev/sda1" "/mnt/boot"
mount_part() {
    printf '%s' "Mounting ${1} at ${2}"
    run_cmd mount -o defaults,noatime,x-mount.mkdir "${1}" "${2}"
}

# --- pre-install ends --- #

# --- install begins --- #

# install_pkgs(): install arch linux by using pacstrap to install necessary
# packages.
#
# use:
#   install_pkgs
install_pkgs() {
    printf '%s' "Installing arch linux packages"
    pacstrap /mnt $(cat "${script_path}/pkgs.base" "${script_path}/pkgs.essentials")
}

# cp_conf(): copy configuration from /config to /etc and /boot.
#
# use:
#   cp_conf
cp_conf() {
    printf '%s' "Copying configuration to /etc"
    run_cmd cp \
        --archive \
        --recursive \
        "${script_path}/config/etc" "/mnt/etc"

    printf '%s' "Copying configuration to /boot"
    run_cmd cp \
        --archive \
        --recursive \
        "${script_path}/config/boot" "/mnt/boot"
}

# gen_locale(): generate locale on the installed system.
#
# use:
#   gen_locale
gen_locale() {
    printf '%s' "Generating locale in chroot"
    run_cmd arch-chroot /mnt locale-gen
}

# set_localtime(): set time and activate NTP on the installed system.
#
# arguments:
#   1 -> timezone
#
# use:
#   set_localtime "Asia/Kolkata"
set_localtime() {
    printf '%s' "Setting ${1} as localtime"
    run_cmd arch-chroot /mnt \
        ln -sf "/usr/share/zoneinfo/${1}" "/etc/localtime" && \
        arch-chroot /mnt hwclock --systohc

    printf '%s' "Activating NTP"
    run_cmd arch-chroot /mnt \
        timedatectl set-ntp true && arch-chroot /mnt timedatectl set-timezone "Asia/Kolkata"
}

# install_bootloader(): install systemd-boot on the installed system.
#
# use:
#   install_bootloader
install_bootloader() {
    printf '%s' "Installing systemd-boot"
    run_cmd arch-chroot /mnt \
        bootctl --path=/boot install
}

# regen_initramfs(): regenerate initramfs on the installed system.
#
# use:
#   regen_initramfs
regen_initramfs() {
    printf '%s' "Regenerating initramfs"
    run_cmd arch-chroot /mnt \
        mkinitcpio -p linux
}

# toggle_services(): activate necesseary services on the installed system.
#
# use:
#   toggle_services
toggle_services() {
    printf '%s' "Enable network manager"
    run_cmd arch-chroot /mnt \
        systemctl enable NetworkManager

    printf '%s' "Masking LVM services"
    run_cmd arch-chroot /mnt \
        systemctl mask lvm2-monitor.service lvm2-lvmpolld.socket
}

# set_root_passwd(): set root password to either something provided in
# /config/root.txt or use "toor", with a warning for the user to change it on
# reboot.
#
# use:
#   set_root_passwd
set_root_passwd() {
    if [ -f "${script_path}/config/root.txt" ]; then
        printf '%s' "Setting password from the provided file"
        arch-chroot /mnt chpasswd $(cat "${script_path}/config/root.txt")
    else
        printf '%s' "Setting default password for root as: toor\nPlease change password or prefrably, lock the root user after reboot."
        printf '%s' "root:toor" | chpasswd
    fi
}

# mk_user(): create an user with sudo privileges, with password taken from
# /config/user.txt.
#
# arguments:
#   1 -> username
#
# use:
#   mk_user "kayg"
mk_user() {
    printf '%s' "Adding user: ${1}"
    run_cmd arch-chroot /mnt \
        useradd \
        --create-home \
        --groups wheel \
        --shell $(command -v zsh) \
        "${1}"


   chpasswd "${script_path}/config/user.txt"
}

# --- install ends --- #

# --- post install begins --- #

# lock_root(): lock the root user account. must only be used after an user has
# been created with sudo privileges.
#
# use:
#   lock_root
lock_root() {
    printf '%s' "Locking the root user account"
    run_cmd passwd --lock root
}

# install_aur_helper(): installs yay, to make it easier for the user to download
# official as well as AUR packages.
#
# use:
#   install_aur_helper
install_aur_helper() {
    printf '%s' "Cloning yay"
    run_cmd git clone 'https://aur.archlinux.org/yay.git' "/tmp/yay"

    printf '%s' 'Building yay'
    cd "/tmp/yay" && \
    makepkg \
        --install \
        --needed \
        --noconfirm \
        --syncdeps
}

# install_extra_pkgs(): install extra packages, including the plasma desktop
# environment.
#
# use:
#   install_extra_pkgs
install_extra_pkgs() {
    printf '%s' "Installing extras"
    yay \
        --needed \
        --noconfirm \
        --refresh \
        --sync \
        --sysupgrade \
        $(cat "${script_path}/pkgs.extra" "${script_path}/pkgs.plasma")
}

# enable_gui_services(): enables services necessary for KDE.
#
# use:
#   enable_gui_services "sddm"
enable_gui_services() {
    printf '%s' "Enabling the following services"
    sudo systemctl enable "${@}"
}

# --- post install ends --- #

# the driver of the script. things get called here.
main() {
    if ! mount | grep --quiet "/dev/mapper/root on /"; then
    # --- pre installation --- #

    # modify the installation env
    set_datetime "Asia/Kolkata"
    set_mirrors "SG" "IN"

    # create a new partition table and make partitions
    wipe_disk "/dev/sda"
    mk_efi_part "1" "512M" "/dev/sda"
    mk_root_part "2" "/dev/sda"

    # create an encrypted drive
    mk_crypt_fs "/tmp/vault/key" "/dev/sda2"
    open_crypt_drive "/tmp/vault/key" "/dev/sda2" "root"

    # create filesystems
    mk_boot_fs "/dev/sda1"
    mk_ext4_fs "/dev/mapper/root"

    # mount partitions
    mount_part "/dev/mapper/root" "/mnt"
    mount_part "/dev/sda1" "/mnt/boot"

    # create and format swapfile
    mk_swap_file "/mnt/swapfile" "4"
    mk_swap_fs "/mnt/swapfile"
    swapon "/mnt/swapfile"

    # --- pre installation --- #

    # --- installation --- #

    install_pkgs
    cp_conf
    gen_locale
    set_localtime "Asia/Kolkata"
    install_bootloader
    toggle_services
    set_root_passwd
    mk_user "kayg"

    # --- installation --- #
    else

    # --- post installation --- #

    # must be run as regular user, functions use sudo where needed
    check_if_root && exit
    install_aur_helper
    install_extra_pkgs
    enable_gui_services "sddm"
    lock_root

    # --- post installation --- #

    fi
}

main "${@}"
